# README #

Push test results to Zephyr via the Zephyr API, a.k.a. ZAPI

Documentation available here:

https://drugdev.atlassian.net/wiki/display/~jeri.levine/Posting+automated+test+results+to+Zephyr+via+ZAPI

You will need to copy over config.yml.sample to config.yml and fill in the template with the actual values from JIRA. How to get these values is covered in the above Confluence document.

You will also need:

- a CSV file with Zephyr test keys and results (PASSED, FAILED) or a log from Turnip (which can be parsed via parse_log.rb)
- a screenshot or other file that can be passed in as an attachment

Right now, since this is just a proof-of-concept, we are using a couple of fake tests (TNP-11205 and TNP-11364) in a fake test cycle ("ZAPI Experiment"). The cycle has been added to version "N/A". These values are currently hard-coded in the ZAPI request code, but in the future we may pass them in as command-line arguments, or figure out how to pass them in from a Jenkins run.