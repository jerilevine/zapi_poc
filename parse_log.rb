require './turnip_result.rb'
require 'csv'

results = []

file = File.new('./turnip.log')

# Each test is delimited by a series of equals signs
lines = file.readlines '========================================'
lines.each do | line |
  # Get rid of lines that include information about setup and teardown
  next if line.include? 'Logfile created'
  next if line.include? 'job daemon'

  # Get the test key
  key = line.match(/(TNP|PAY|SAM)(-\d+)/) { |m| m.captures.join }

  # This is really horrible, so I am just going to outline it.
  # 1. Get anything that looks like "I, [2017-06-12T16:48:53.598642 #9366]  INFO -- : the "TNP-10005 Checklist" page appears: As expected. PASS"
  # 2. Grab all the stuff that comes after the first colon because that's the step description and status
  # 3. Take off the bit that goes "As expected" and split the descriptions from the PASS|FAILED|PENDING results
  # 4. The first line is the individual test setup and the last line is the individual teardown, so we'll leave those off.
  parts = line.scan(/\s:\s(.*)\n/)
              .flatten
              .delete_if { |desc| (desc.start_with? 'Starting') || (desc.start_with? 'Screenshot') || (desc.end_with? 'completed.') }
  first_fail = parts.index { |p| (p.include? 'PENDING') || (p.include? 'FAILED') } || parts.size
  steps_results = parts.slice(0..first_fail)
                        .collect { |desc| desc.split(': ')
                                              .collect { |desc| desc.gsub('As expected. ', '') } }

  result = TurnipResult.new(key: key, steps_results: steps_results)
  results << result
end

CSV.open('tmp.csv', 'w') do | csv |
  results.each do | result |
    csv << [ result.key, result.result ]
  end
end
