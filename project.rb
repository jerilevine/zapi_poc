class Project
  attr_accessor :id, :key, :versions
  
  def initialize(params={})
    @id = params[:id]
    @key = params[:key]
    @versions = params[:versions]
  end

  def add_version(version_id, version_name)
    @versions << { version_id => version_name }
  end
end
