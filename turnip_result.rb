class TurnipResult
  attr_accessor :key, :steps_results

  def initialize(params={})
    @key = params[:key]
    @steps_results = params[:steps_results]
  end

  def result
    if @steps_results.collect { |r| r[1] }.include? 'FAILED'
      return 'FAILED'
    elsif @steps_results.collect { |r| r[1] }.include? 'PENDING'
      return 'PENDING'
    else return 'PASSED'
    end
  end
end
