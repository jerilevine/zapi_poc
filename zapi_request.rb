require 'json'
require 'jwt'
require 'rest-client'
require 'uri'
require 'yaml'
require './project.rb'

# Get the results CSV, which is of the format
# test_key, PASSED|FAILED
# into a hash that contains { test_key => PASS|FAIL }
lines = File.new('./turnip.csv', 'r').readlines
test_keys_results = lines.collect { |l| l.rstrip.gsub('ED', '').split(',') }.to_h
filename = 'screenshot.png'

version = 'N/A'
cycle_name = 'ZAPI Experiment'

################################################################################
# Config
################################################################################

def do_config
  config = YAML.load_file(File.expand_path('../config.yml', __FILE__))

  @access_key = config['access_key']
  @secret_key = config['secret_key']
  @user_name = config['user_name']
  @password = config['password']
  @zapi_base_url = config['zapi_base_url']
  @jira_base_url = config['jira_base_url']
end

################################################################################
# Generate JWT token. See also
# https://developer.atlassian.com/static/connect/docs/latest/concepts/understanding-jwt.html
# and
# https://support.getzephyr.com/hc/en-us/articles/115000308283-401-error-while-making-ZAPI-Cloud-API-requests
#
# method:      Symbol (one of :get, :put, :post, :delete)
# request_uri: URI
################################################################################

def generate_jwt(method, request_uri)
  path = request_uri.to_s.gsub(@zapi_base_url, '').gsub(request_uri.query || '', '').gsub('?', '')
  
  # Request parameters (if any) must be sorted by key
  query = URI.encode_www_form(URI.decode_www_form(request_uri.query).sort) unless request_uri.query.nil?

  unencoded_qsh = "#{method.to_s.upcase}&#{path}&#{query}"

  payload = {
    'sub' => @user_name,
    'iss' => @access_key,
    'exp' => (Time.now + 3600).to_i,
    'qsh' => Digest::SHA2.new(256).hexdigest(unencoded_qsh.encode('utf-8')),
    'iat' => Time.now.to_i
  }

  token =  "JWT " + JWT.encode(payload, @secret_key, 'HS256')

  return token
end

################################################################################
# Send request to ZAPI
#
# method:      Symbol (one of :get, :put, :post, :delete)
# request_uri: URI
# payload:     Usually JSON.
################################################################################

def send_zapi_request(method, request_uri, payload=nil, content_type=nil)
  token = generate_jwt(method, request_uri)
  headers = {
    'Authorization' => token,
    'zapiAccessKey' => @access_key
  }
  headers['Content-Type'] = content_type unless content_type.nil?

  res = RestClient::Request.execute(method: method, url: request_uri.to_s, headers: headers, payload: payload)

  if res.code >= 300
    raise "Request failed with code #{res.code}"
  end

  return JSON.parse(res.body)
end

################################################################################
# Send JIRA request
################################################################################

def send_jira_request(method, request_uri, payload=nil, content_type=nil)
  auth_string = "Basic " + Base64.encode64("#{@user_name}:#{@password}")

  headers = {
    'Authorization' => auth_string
  }
  headers['Content-Type'] = content_type unless content_type.nil?

  res = RestClient::Request.execute(method: method, url: request_uri.to_s, headers: headers, payload: payload)

  if res.code >= 300
    raise "Request failed with code #{res.code}"
  end

  return JSON.parse(res.body)
end

################################################################################
# Formulate ZAPI URI from path and parameters
################################################################################

def create_zapi_uri(path, params=nil)
  uri = URI(@zapi_base_url + path)
  uri.query = URI.encode_www_form(params) unless params.nil?

  return uri
end

################################################################################
# Formulate JIRA URI from path and parameters
################################################################################

def create_jira_uri(path, params=nil)
  uri = URI(@jira_base_url + path)
  uri.query = URI.encode_www_form(params) unless params.nil?

  return uri
end

################################################################################
# Get project ID and versions for a project by key
################################################################################

def get_project(project_key)
  path = "/rest/api/2/project/#{project_key}/versions"
  uri = create_jira_uri(path)

  json = send_jira_request(:get, uri)
  project_id = json.first['projectId']                     # Must be exactly one

  versions = {}
  json.each { |j| versions[j['name']] = j['id'] }     # Must be at least one

  project = Project.new(id: project_id, key: project_key, versions: versions)
  
  return project
end

################################################################################
# Get project IDs and versions for a list of project keys
################################################################################

def get_projects_info(project_keys)
  projects = []
  
  project_keys.each do | project_key |
    projects << get_project(project_key)
  end

  return projects
end

################################################################################
# Get all tests
# JIRA limits query result downloads to 1000 records, so we will need to use
# pagination.
################################################################################

def get_tests_info(test_keys_list)
  path = '/rest/api/2/search'
  # This looks horrible but I'm just replacing square brackets with parentheses
  keys_query_string = test_keys_list.to_s.tr('[','(').tr(']',')')
  params = { :jql => "issueType = Test and key in #{keys_query_string}" }
  
  uri = create_jira_uri(path, params)

  # TODO pagination
  json = send_jira_request(:get, uri)

  return json['issues'].collect { |j| { j['key'] => j['id'] } }
end

################################################################################
# Get test id for test key
################################################################################

def get_test_id(test_key)
  path = '/rest/api/2/search'
  params = { :jql => "issueType = Test and key = #{test_key}" }
  
  uri = create_jira_uri(path, params)

  # TODO pagination
  json = send_jira_request(:get, uri)

  return json['issues'].first['id']
end

################################################################################
# Search all test cycles
################################################################################

def get_test_cycles(version_id, project_id)
  path = '/public/rest/api/1.0/cycles/search'
  params = { :versionId => version_id,
             :projectId => project_id
           }
  uri = create_zapi_uri(path, params)

  json = send_zapi_request(:get, uri)

  return json
end

################################################################################
# Get cycle ID for named test cycle
################################################################################

def get_test_cycle(version_id, project_id, cycle_name)
  test_cycles = get_test_cycles(version_id, project_id)

  begin
    cycle = test_cycles.select { |cycle| cycle['name'] == cycle_name }.first
  rescue NoMethodError, TypeError => e     # we didn't find the cycle
    puts "Encountered error #{e.message}"
    raise "Couldn't find a cycle named #{cycle_name}"
  end

  return cycle
end

################################################################################
# Get step results for test executions
################################################################################

def get_step_results(issue_id, execution_id)
  path = '/public/rest/api/1.0/stepresult/search'
  params = { :executionId => execution_id,
             :issueId     => issue_id
           }
  uri = create_zapi_uri(path, params)

  json = send_zapi_request(:get, uri)

  return json
end

################################################################################
# Get statuses
################################################################################

def get_status_info
  path = '/public/rest/api/1.0/execution/statuses'
  uri = create_zapi_uri(path)

  json = send_zapi_request(:get, uri)

  status_hash = { }
  json.each do |j|
    status_hash[j['name']] = j['id']
  end

  return status_hash
end

################################################################################
# POST new test execution
################################################################################

def post_test_execution(project_id, issue_id, version_id, status_id=1, cycle_id=nil)
  path = '/public/rest/api/1.0/execution'
  uri = create_zapi_uri(path)

  payload_hash = {
    'status'    => { 'id' => status_id },
    'projectId' => project_id,
    'issueId'   => issue_id,
    'versionId' => version_id
  }
  payload_hash['cycleId'] = cycle_id unless cycle_id.nil?

  payload_json = payload_hash.to_json

  return send_zapi_request(:post, uri, payload_json, 'application/json')
end

################################################################################
# Update step results
################################################################################

def update_step_result(step_result_id, status_id, issue_id, execution_id)
  path = "/public/rest/api/1.0/stepresult/#{step_result_id}"
  uri = create_zapi_uri(path)

  # stepId is actually an optional parameter, but we're skipping it
  # since Turnip steps don't necessarily match 1:1 with Zephyr steps
  # If the test passes in Turnip, all the steps pass.
  # If a test fails, a human should update the status
  payload_hash = {
    'status'      => { 'id' => status_id },
    'issueId'     => issue_id,
    'executionId' => execution_id
  }
  payload_json = payload_hash.to_json

  return send_zapi_request(:put, uri, payload_json, 'application/json')
end

################################################################################
# Upload screenshot as attachment
################################################################################

def upload_screenshot(issue_id, version_id, cycle_id, entity_id, project_id, filename)
  path = '/public/rest/api/1.0/attachment'

  #entityName=execution
  #entityId=   0001498246789874-242ac112-0001
  #executionId=0001498246789874-242ac112-0001
  #projectId=11000
  #issueId=54418
  #cycleId=0001496327503098-242ac112-0001
  #versionId=10801
  params = { :issueId     => issue_id,
             :versionId   => version_id,
             :cycleId     => cycle_id,
             :entityName  => 'execution',     # Either 'execution' or 'stepResult'
             :entityId    => entity_id,       # Either execution_id or step_result_id
             :executionId => entity_id,       # this seems super redundant but whatever
             :projectId   => project_id,
             :comment     => ''               # docs said this was optional. apparently that is incorrect.
           }

  uri = create_zapi_uri(path, params)

  file = File.new(filename, 'rb')
  payload = { :multipart => true,
              :file => file
            }

  json = send_zapi_request(:post, uri, payload, 'multipart/form-data')
end

################################################################################
# Main section
################################################################################

do_config

status_hash = get_status_info

test_keys_results.each do | test_key, test_result |
  # Get prerequisites:
  # project IDs
  # version IDs
  # Zephyr test case IDs
  # test cycle IDs

  test_id = get_test_id(test_key)
  project = get_project(test_key.split('-')[0])
  version_id = project.versions[version]
  cycle_id = get_test_cycle(version_id, project.id, cycle_name)['cycleIndex']
  status_id = status_hash[test_result]

  # Create a test execution
  execution_json = post_test_execution(project.id, test_id, version_id, status_id, cycle_id)
  execution_id = execution_json['execution']['id']
  
  # Get the step results for the test execution
  step_results_json = get_step_results(test_id, execution_id)
  step_result_ids = step_results_json['stepResults'].collect { |r| r['id'] }
  
  # Populate the execution with the results
  step_result_ids.each do | step_result_id |
    update_step_result(step_result_id, status_id, test_id, execution_id)
    upload_screenshot(test_id, version_id, cycle_id, execution_id, project.id, filename)
  end
end

